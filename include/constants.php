<?php
define('HOST', 'http://'.$_SERVER['HTTP_HOST']);
define('MAIL', 'test@myonlinebooks.com');
define('API', HOST.'/rest/api.php?');
define('IMAGE_DIR', $_SERVER['DOCUMENT_ROOT'].'/books/covers/');
define('FILE_DIR', $_SERVER['DOCUMENT_ROOT'].'/books/files/');
define('IMAGE', HOST.'/books/covers/');
define('FILE', HOST.'/books/files/');

?>