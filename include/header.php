<?php

session_start();

if (isset($_GET['logout'])) {
  unset($_SESSION['login']);
}

// On vérifie si l'utilisateur est connecté
if (isset($_SESSION['login'])) {
  $user_logged = json_decode(CallAPI("GET", API."function=getUser&id=".$_SESSION['login']));
}

// On essaye de se connecter
if (isset($_POST['login_nickname'])) {
  // On traite les caractères spéciaux par sécurité
  $login_nickname = htmlspecialchars($_POST['login_nickname']);
  $login_password = htmlspecialchars($_POST['login_password']);
  // On vérifie si le user existe
  $user = json_decode(CallAPI("GET", API."function=getUser&nickname=".$login_nickname));
  if ($user != null) {
    // Check combinaison mot de passe login
    if (password_verify($login_password,$user->password)) {
      $login_valid = "<div class='div-message success'>Vous êtes maintenant connecté à votre compte.</div>";
      $_SESSION['login'] = $user->id;
    }
    else {
      $login_error = "<div class='div-message error'>Erreur, login ou mot de passe invalide.</div>";
    }
  }
  else {
    $login_error = "<div class='div-message error'>Erreur, login ou mot de passe invalide.</div>";
  }
}

if (isset($_GET['page'])) {
  // défini quelle page va être affichée et pour chaque page un titre et une description
$page = $_GET['page'];
  switch ($page) {
      case "list":
        $title = "Liste des livres";
        $description = "Découvrez la liste de tous nos livres libres de droits";
        break;
      case "contact":
        $title = "Contact";
        $description = "Une question, une suggestion, un bug découvert sur le site ? Contactez-nous";
        break;
      case "about":
        $title = "A Propos";
        $description = "Qui sommes nous, découvrez l'équipe de MyOnlineBooks";
        break;
      case "register":
        $title = "Créer un compte";
        $description = "Créez votre compte pour ajouter vos livres préférés.";
        break;
      case "signin":
        $title = "Se connecter";
        $description = "Se connecter à votre compte";
        break;
      case "account":
        $title = "Mon compte";
        $description = "Vérifier les détails de mon compte";
        break;
      case "addbook":
        $title = "Ajouter un livre";
        $description = "Suggerer un livre à l'équipe de MyOnlineBooks";
        break;
      case "book":
      // Si on est dans une page livre, on va maintenant chercher les infos du livre pour définir les metas et ainsi favoriser le SEO
        if (isset($_GET['bookid'])) {
          $book = json_decode(CallAPI("GET", API."function=getBook&id=".$_GET['bookid']));
          if (isset($book)) {
            $category = json_decode(CallAPI("GET", API."function=getCategory&id=".$book->category_id));
            $title = "Livre ".$book->name." (".$category->name.")" ;
            $description = $book->description;
          }
          else {
            $title = "Oups ! Livre non trouvé.";
            $description = "Le livre recherché n'a pas été trouvé.";
          }
        }
        else {
          $title = "Oups ! Livre non trouvé.";
          $description = "Le livre recherché n'a pas été trouvé.";
        }
      break;
    }
}
else {
  $page = "home";
  $title = "Accueil";
  $description = "MyOnlineBooks est une bibliothèque en ligne de livres d'informatique";
}


$title .= " - MyOnlineBooks";
?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
      <meta name="description" content="<?php echo $description ?>">
      <title><?php echo $title ?></title>
      <link type="text/css" rel="stylesheet" href="<?php echo HOST ?>/css/style.css"  media="screen,projection"/>
      <script src="<?php echo HOST ?>/libraries/jquery/jquery-3.1.0.min.js"></script>
      <script src="<?php echo HOST ?>/js/script.js"></script>
    </head>
    <body>