<div class="menu-icon" onclick="toggleMenu()">
    menu
</div>
<div class="menu">
<div class="text-logo">MyOnlineBooks</div>
  <ul id="menu" class="menu-hide">
    <li><a <?php if ($page == "home") {echo "class='active'";} ?> href="/">Accueil</a></li>
    <li><a <?php if ($page == "list") {echo "class='active'";} ?> href="/list">Rechercher un livre</a></li>
    <li><a <?php if ($page == "contact") {echo "class='active'";} ?> href="/contact">Contact</a></li>
    <li><a <?php if ($page == "about") {echo "class='active'";} ?> href="/about">A propos</a></li>
    
    <?php
    if (isset($user_logged)) {
        ?>
        <li>
            <a <?php if ($page == "addbook") {echo "class='active'";} ?> href="/addbook">Ajouter un livre</a>
        </li>
        <li>
            <a <?php if ($page == "account") {echo "class='active'";} ?> href="account" id="accountLink" ><?php echo $user_logged->nickname ?></a>
        </li>
        <?php
    }
    else {
        ?>
        <li>
            <a <?php if ($page == "signin") {echo "class='active'";} ?> href="#" onclick="toggleSignIn()" id="accountLink" >Mon compte</a>
            <div id="signin" class="signin-hide">
                <form action="/signin" method="POST">
                    <input type="text" name="login_nickname" id="nickname" placeholder="Pseudo"><br>
                    <input type="password" name="login_password" id="password" placeholder="Mot de passe"><br>
                    <button type="submit">Connexion</button>
                    <p class="menu-mini-text"><a href="/register">Pas encore de compte ?</a></p>
                </form>
            </div>
        </li>
        <?php
    }
     ?>
    	
  </ul>
</div>