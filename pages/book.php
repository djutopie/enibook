
<main>
<?php
// On a bien trouvé le livre recherché
if (isset($book)) { ?>
	<h1><?php echo $book->name ?></h1>
	<h2 class="text-author">Par <?php echo $book->author; ?></h2>
	<div class="row">
		<div class="col m12 d4">
			<p><img class="img-bookcover-big" src="<?php echo IMAGE.$book->image_url ?>"></p>
		</div>
		<div class="col m12 d8">
			<div class="text-book-description">
				<p><a target="_blank" href="<?php echo FILE.$book->file_url ?>">Lire maintenant</a></p>
				<p>Année de parution : <?php echo date("Y",(int)$book->release_date) ?></p>
				<p><a href="/list/<?php echo $category->short_url ?>"><span class="label default"><?php echo $category->name ?></span></a></p>
				<p><?php echo $book->description ?></p>
			</div>
		</div>
	</div>
<?php }
// Le livre n'a pas été trouvé ou la variable get n'est pas définie
else { ?>
	<h1>Livre non trouve</h1>
	<p>Le livre recherché n'a pas été trouvé.</p>
	<p><a href="/list">Rechercher un autre livre</a></p>
<?php } ?>

</main>