<main>
	<h1>Mon compte</h1>
	

<?php
// Demande de suppression de compte
if (isset($_POST['delete'])) {

	$link = mysqli_connect($server, $user, $password, $base);
	
	// On récupère l'id depuis la variable de session
	$id = $user_logged->id;

	// Check connection
	if($link === false){
		error_log("Impossible de se connecter à la base de données " . mysqli_connect_error());
		die("<div class='div-message error'>Une erreur est survenue, compte n'a pas été supprimé.</div>");
	}
	$sql = "DELETE FROM user WHERE id = '$id'";
	if(!mysqli_query($link, $sql)){
		error_log("Impossible d'executer la requête " .mysqli_error($link));
		echo "Impossible d'executer la requête " .mysqli_error($link);
    }
    else {
    	unset($_SESSION['login']);
    	echo "<div class='div-message success'>Votre compte a bien été supprimé.<br> Vous avez été déconnecté.</div>";
    	echo "<p><a href='/'>Cliquez ici pour revenir à la page d'accueil.</a></p>";
    }

     mysqli_close($link);
}

?>

	<h2>Vérifier les détails de mon compte.</h2>
	<p>Mon login : <?php echo $user_logged->nickname; ?></p>
	<p>Mon adresse mail : <?php echo $user_logged->mail; ?></p>
	<p><a href="/logout">Me déconnecter</a></p>
	<p><a href="#" onclick="$('#delconfirm').show('slow')">Supprimer mon compte</a></p>
	<div id="delconfirm" style="display:none" class="div-confirm">
		<p><span class="text-confirm">Etes vous sur de vouloir supprimer votre compte ?<br>
		Toutes vos informations seront supprimées mis à part les livres que vous avez ajouté.<br></span></p>
		<p><form action="/account" method="POST"><input type="hidden" name="delete"><button type="submit">Je confirme la suppression de mon compte</button><a href="#" onclick="$('#delconfirm').hide('slow')">Annuler</a></form></p>
	</div>
</main>
