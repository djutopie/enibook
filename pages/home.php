<?php
$books = json_decode(CallAPI("GET", API."function=getBooks"));
$categories = json_decode(CallAPI("GET", API."function=getCategories"));
shuffle($books);

?>

<main>
	<h1>Accueil</h1>
	<div class="div-big">
		<p>Bienvenue sur <span class="text-mini-logo">MyOnlineBooks</span>, le numéro 1 de la bibliothèque en ligne, de livres libres de droits.</p>
	</div>
	<div class="div-big">
		<p>Les livres à découvrir :</p>
		<div class="row">
			<div class="col m12 d4 center book">
				<p><a href="/book/<?php echo $books[0]->id ?>"><img src="<?php echo IMAGE.$books[0]->image_url ?>" class="img-bookcover"></a></p>
				<p class="text-bookcover"><?php echo $books[0]->name ?></p>
			</div>
			<div class="col m12 d4 center book">
				<p><a href="/book/<?php echo $books[1]->id ?>"><img src="<?php echo IMAGE.$books[1]->image_url ?>" class="img-bookcover"></a></p>
				<p class="text-bookcover"><?php echo $books[1]->name ?></p>
			</div>
			<div class="col m12 d4 center book">
				<p><a href="/book/<?php echo $books[2]->id ?>"><img src="<?php echo IMAGE.$books[2]->image_url ?>" class="img-bookcover"></a></p>
				<p class="text-bookcover"><?php echo $books[2]->name ?></p>
			</div>
		</div>
	</div>
	<div class="div-big">
		<p><span class="text-mini-logo">MyOnlineBooks</span> en chiffres :</p>
		<div class="row">
			<div class="col m12 d4 center">
				<span class="text-mega-big"><?php echo count($books); ?></span><br>Livres
			</div>
			<div class="col m12 d4 center">
				<span class="text-mega-big"><?php echo count($categories); ?></span><br>Catégories
			</div>
			<div class="col m12 d4 center">
				<span class="text-mega-big">1</span><br>Lecteur(s) en ligne
			</div>
		</div>
	</div>
	
</main>