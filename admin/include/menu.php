<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Affichage mobile -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">MyOnlineBooks Admin</a>
    </div>

    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <li <?php if ($page=='updatebook') echo 'class="active"'; ?>><a href="index.php?page=updatebook" >Ajouter/Modifier un livre</a></li>
        <li <?php if ($page=='listbooks') echo 'class="active"'; ?>><a href="index.php?page=listbooks">Lister les livres</a></li>
        <li <?php if ($page=='updatecategory') echo 'class="active"'; ?>><a href="index.php?page=updatecategory">Ajouter/Modifier une catégorie</a></li>
        <li <?php if ($page=='listcategories') echo 'class="active"'; ?>><a  href="index.php?page=listcategories">Lister les catégories</a></li>
      </ul>
    </div>
  </div>
</nav>