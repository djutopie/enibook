<?php
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}
else {
  $page = "home";
}
?>
<!DOCTYPE html>
  <html>
    <head>
    	<title>Admin - MyOnlineBooks</title>
		<meta charset="utf-8">
		<link href="libraries/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
      .img-miniature {
        height: 50px;
        width: 50px;
      }
    </style>
     </head>
     <body>
      <script src="libraries/jquery-1.12/jquery-1.12.4.min.js"></script>
      <script src="libraries/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>