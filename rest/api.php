<?php

function sql_connect() {
    if ($_SERVER['HTTP_HOST'] == "enibook.local") {
        return new mysqli("localhost", "dju", "toto", "myonlinebooks");
    }
    return new mysqli("localhost", "dju", "blogibloga2", "myonlinebooks");
}

// Recupere une categorie en fonction de son id ou de son url
function getCategory() {
    $mysqli = sql_connect();

    if (isset($_GET['url'])) {
      // On fait attention aux injections sql
      $url = mysqli_real_escape_string($mysqli,$_GET['url']);
      $req = "WHERE short_url = '".$url."'";
    }
    else if (isset($_GET['id'])) {
      // On fait attention aux injections sql
      $id = mysqli_real_escape_string($mysqli,$_GET['id']);
      $req = "WHERE id = '".$id."'";
    }
    else {
      http_response_code(400);
      exit("Erreur de requête");
    }

    

    /* Vérification de la connexion */
    if ($mysqli->connect_errno) {
        http_response_code(500);
        exit("Impossible de se connecter à la base de données");
    }

    $query = "SELECT * FROM category $req" ;
    
    $result = $mysqli->query($query);
      //echo $result->fetch_array();
    $row = $result->fetch_array(MYSQLI_ASSOC);

    /* Libération des résultats */
    $result->close();
      /* Fermeture de la connexion */
    $mysqli->close();
    return  $row;
} 

// Recupere toutes les catégories
function getCategories() {
  $mysqli = sql_connect();

  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM category ORDER BY name" ;
  $result = $mysqli->query($query);

  /* On parcours les résultats et on les stocke */
  while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  /* Libération des résultats */
  $result->close();
  /* Fermeture de la connexion */
  $mysqli->close();

  return  $rows;
}

// Récupère un livre par son id
function getBook() {
    $mysqli = sql_connect();

    if (isset($_GET['id'])) {
      // On fait attention aux injections sql
      $id = mysqli_real_escape_string($mysqli,$_GET['id']);
      $req = "AND id = '".$id."'";
    }
    else {
      http_response_code(400);
      exit("Erreur de requête");
    }


    /* Vérification de la connexion */
    if ($mysqli->connect_errno) {
        http_response_code(500);
        exit("Impossible de se connecter à la base de données");
    }

    $query = "SELECT * FROM book WHERE approved = 1  $req" ;
    
    $result = $mysqli->query($query);
      //echo $result->fetch_array();
    $row = $result->fetch_array(MYSQLI_ASSOC);

    /* Libération des résultats */
    $result->close();
      /* Fermeture de la connexion */
    $mysqli->close();
    return  $row;
} 

// Récupère un livre par son id
function getAnyBook() {
    $mysqli = sql_connect();

    if (isset($_GET['id'])) {
      // On fait attention aux injections sql
      $id = mysqli_real_escape_string($mysqli,$_GET['id']);
      $req = "WHERE id = '".$id."'";
    }
    else {
      http_response_code(400);
      exit("Erreur de requête");
    }


    /* Vérification de la connexion */
    if ($mysqli->connect_errno) {
        http_response_code(500);
        exit("Impossible de se connecter à la base de données");
    }

    $query = "SELECT * FROM book $req " ;
    
    $result = $mysqli->query($query);
      //echo $result->fetch_array();
    $row = $result->fetch_array(MYSQLI_ASSOC);

    /* Libération des résultats */
    $result->close();
      /* Fermeture de la connexion */
    $mysqli->close();
    return  $row;
} 

// Récupère tous les livres approuvés, optionnel : par category id
function getBooks() {
  $mysqli = sql_connect();

  if (isset($_GET['category_id'])) {
      // On fait attention aux injections sql
      $category_id = mysqli_real_escape_string($mysqli,$_GET['category_id']);
      $req = "AND category_id = '".$category_id."'";
    }


  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM book WHERE approved = 1 $req ORDER BY name " ;
  $result = $mysqli->query($query);

  /* On parcours les résultats et on les stocke */
  while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  /* Libération des résultats */
  $result->close();
  /* Fermeture de la connexion */
  $mysqli->close();

  return  $rows;
}

// Récupère tous les livres, optionnel : par category id
function getAllBooks() {
  $mysqli = sql_connect();

  if (isset($_GET['category_id'])) {
      // On fait attention aux injections sql
      $category_id = mysqli_real_escape_string($mysqli,$_GET['category_id']);
      $req = "WHERE category_id = '".$category_id."'";
    }


  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM book $req ORDER BY name " ;
  $result = $mysqli->query($query);

  /* On parcours les résultats et on les stocke */
  while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  /* Libération des résultats */
  $result->close();
  /* Fermeture de la connexion */
  $mysqli->close();

  return  $rows;
}

// Récupère tous les livres, optionnel : par category id
function getNonApprovedBooks() {
  $mysqli = sql_connect();

  if (isset($_GET['category_id'])) {
      // On fait attention aux injections sql
      $category_id = mysqli_real_escape_string($mysqli,$_GET['category_id']);
      $req = "AND category_id = '".$category_id."'";
    }


  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM book WHERE approved = 0 $req ORDER BY name" ;
  $result = $mysqli->query($query);

  /* On parcours les résultats et on les stocke */
  while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  /* Libération des résultats */
  $result->close();
  /* Fermeture de la connexion */
  $mysqli->close();

  return  $rows;
}

function searchBooks() {

  $mysqli = sql_connect();

  if (isset($_GET['name'])) {
    // On fait attention aux injections sql
    $name = mysqli_real_escape_string($mysqli,$_GET['name']);
    $req = "AND name LIKE '%".$name."%'";
  }

  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM book WHERE approved = 1  $req ORDER BY name" ;
  $result = $mysqli->query($query);

  /* On parcours les résultats et on les stocke */
  while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $rows[] = $row;
  }

  /* Libération des résultats */
  $result->close();
  /* Fermeture de la connexion */
  $mysqli->close();

  return  $rows;
}

function getUser() {
  $mysqli = sql_connect();

  $req = "";

  if (isset($_GET['nickname'])) {
    // On fait attention aux injections sql
    $nickname = mysqli_real_escape_string($mysqli,$_GET['nickname']);
    $req = "WHERE nickname = '".$nickname."'";
  }
  else if (isset($_GET['mail'])) {
    // On fait attention aux injections sql
    $mail = mysqli_real_escape_string($mysqli,$_GET['mail']);
    $req = "WHERE mail = '".$mail."'";
  }
  else if (isset($_GET['id'])) {
    // On fait attention aux injections sql
    $id = mysqli_real_escape_string($mysqli,$_GET['id']);
    $req = "WHERE id = '".$id."'";
  }


  /* Vérification de la connexion */
  if ($mysqli->connect_errno) {
      http_response_code(500);
      exit("Impossible de se connecter à la base de données");
  }

  /* requete */
  $query = "SELECT * FROM user $req LIMIT 1" ;

  $result = $mysqli->query($query);
    //echo $result->fetch_array();
  $row = $result->fetch_array(MYSQLI_ASSOC);

  /* Libération des résultats */
  $result->close();
    /* Fermeture de la connexion */
  $mysqli->close();
  return  $row;
}



if($_GET['function']!='') {
   $value = @call_user_func($_GET['function'], $_GET['arg']); 
   header('Content-Type: application/json');
   echo json_encode($value); 
}

else {
   echo "<response>
            <error>".utf8_encode("Requête Invalide")."</error>
            <function>".$_GET['function']."</function>
            <argument>".$_GET['arg']."</argument>
         </response>";
}
?>
